FROM ubuntu:18.04

# TODO: Instalar telnet
RUN apt -y update
RUN apt -y upgrade
RUN apt -y install git swig g++-6 m4 python-pip libgoogle-perftools-dev g++ zlib1g-dev
RUN apt -y install gcc-arm-linux-gnueabihf autoconf libtool wget make
RUN apt -y clean

RUN pip install scons==2.5.1
ENV SCONS_LIB_DIR="/usr/local/lib/python2.7/dist-packages/scons-2.5.1"
# Comando para compilar o gem5:
# CC=gcc-6 CXX=g++-6 scons build/ARM/gem5.opt -j 8

# Instalando o wolfssl.
WORKDIR /usr/local/src
RUN git clone https://github.com/wolfssl/wolfssl.git
WORKDIR /usr/local/src/wolfssl
RUN ./autogen.sh
# Instalando a biblioteca no compilador padrão (gcc)
RUN ./configure --enable-sha3 --enable-sha512 --enable-ecc --enable-ecccustcurves --enable-eccencrypt CFLAGS="-DHAVE_ECC_KOBLITZ"
RUN make
RUN make install
RUN ldconfig
# Instalando a biblioteca no compilador arm (arm-linux-gnueabihf-gcc)
RUN ./configure --host=arm-linux CC=arm-linux-gnueabihf-gcc LD=arm-linux-gnueabihf-ld AR=arm-linux-gnueabihf-ar RANLIB=arm-linux-gnueabihf-ranlib --prefix='/usr/arm-linux-gnueabihf/' --enable-static --enable-sha3 --enable-sha512 --enable-ecc --enable-ecccustcurves --enable-eccencrypt CFLAGS="-DHAVE_ECC_KOBLITZ" CPPFLAGS="-I./"
RUN make
RUN make install
RUN ldconfig

ENTRYPOINT bash
