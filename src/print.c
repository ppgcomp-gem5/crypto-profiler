#include <stdio.h>

#include "print.h"

void printBuffer(char* name, uint8_t* buffer, size_t len)
{
    printf("%s[%d] = { ", name, len);

    int i;
    for(i = 0; i < len; i++)
        printf("%d, ", buffer[i]);

    printf("}\n");
}
