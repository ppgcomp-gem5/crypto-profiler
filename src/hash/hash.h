#include <stdint.h>
#include <stddef.h>

extern const int hashSize;

int calcHash(uint8_t* data, size_t dataLen, uint8_t* out, size_t outLen);
