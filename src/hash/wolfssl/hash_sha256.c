#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha256.h>

#include "../hash.h"

int calcHash(uint8_t* data, size_t dataLen, uint8_t* out, size_t outLen)
{
    Sha256 sha;
    wc_InitSha256(&sha);
    wc_Sha256Update(&sha, data, dataLen);
    wc_Sha256Final(&sha, out);

    wc_Sha256Free(&sha);

    return SHA256_DIGEST_SIZE;
}
