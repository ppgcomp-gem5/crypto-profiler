#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha512.h>

#include "../hash.h"

int calcHash(uint8_t* data, size_t dataLen, uint8_t* out, size_t outLen)
{
    Sha512 sha;
    wc_InitSha512(&sha);
    wc_Sha512Update(&sha, data, dataLen);
    wc_Sha512Final(&sha, out);

    wc_Sha512Free(&sha);

    return SHA512_DIGEST_SIZE;
}
