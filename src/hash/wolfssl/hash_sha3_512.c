#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha3.h>

#include "../hash.h"

int calcHash(uint8_t* data, size_t dataLen, uint8_t* out, size_t outLen)
{
    Sha3 sha;
    wc_InitSha3_512(&sha, NULL, 0);
    wc_Sha3_512_Update(&sha, data, dataLen);
    wc_Sha3_512_Final(&sha, out);

    wc_Sha3_512_Free(&sha);

    return SHA3_512_DIGEST_SIZE;
}
