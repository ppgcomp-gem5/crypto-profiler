#include <stdio.h>

#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha256.h>

#include "payload.h"

int main()
{
    byte shaSum[SHA256_DIGEST_SIZE];   
    int i;   

    Sha256 sha;
    wc_InitSha256(&sha);
    wc_Sha256Update(&sha, payload, sizeof(payload));
    wc_Sha256Final(&sha, shaSum);

    printf("Hash SHA256:\n");

    for(i = 0; i < SHA256_DIGEST_SIZE; i++)
        printf("[%02d] - %d\n", i, shaSum[i]);
}
