#include <stdint.h>
#include <stddef.h>

extern const int dataSize;

int getData(uint8_t* buffer, size_t len);
void processData(uint8_t* buffer, size_t len);