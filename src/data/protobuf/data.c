#include <string.h>

#include "../data.h"

// {"data": "Exemplo de payload!"}
// Serializado utilizando protobuf através da ferramento online http://yura415.github.io/js-protobuf-encode-decode/

static uint8_t payload[] = 
{
    0x0A, 0x13, 0x45, 0x78, 0x65, 
    0x6D, 0x70, 0x6C, 0x6F, 0x20, 
    0x64, 0x65, 0x20, 0x70, 0x61, 
    0x79, 0x6C, 0x6F, 0x61, 0x64, 
    0x21
};

int getData(uint8_t* buffer, size_t len)
{
    if(len < sizeof(payload))
        return sizeof(payload) - len;
        
    memcpy(buffer, payload, sizeof(payload));
    return sizeof(payload);
}

void processData(uint8_t* buffer, size_t len)
{
    // Não faz nenhum processamento.
}
