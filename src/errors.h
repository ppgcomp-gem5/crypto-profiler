#define ERROR_DATA      -1
#define ERROR_HASH      -2
#define ERROR_SIGNATURE -3

#ifdef PRINT
#define HANDLE_ERROR(error, code)           \
    printf("ERRO %d (%d)\n", error, code);  \
    return error
#else
#define HANDLE_ERROR(error, code) return error
#endif
