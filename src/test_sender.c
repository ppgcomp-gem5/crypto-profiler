#include "data/data.h"
#include "errors.h"
#include "hash/hash.h"
#include "send/send.h"
#include "sign/sign.h"

#ifdef PRINT
#include <stdio.h>
#include "print.h"
#endif

#define DATA_SIZE 256
#define HASH_SIZE   256
#ifdef SIGN_DATA
#define SIGNATURE_SIZE    512
#endif

#ifndef LOOP_COUNT
#define LOOP_COUNT  1
#endif

int test()
{
    uint8_t data[DATA_SIZE];
    int dataLen = getData(data, DATA_SIZE);

    if(dataLen < 0)
    {
        HANDLE_ERROR(ERROR_DATA, dataLen);
    }

#ifdef PRINT
    printBuffer("DATA", data, dataLen);
#endif

    uint8_t hash[HASH_SIZE];
    int hashLen = calcHash(data, dataLen, hash, HASH_SIZE);

    if(hashLen < 0)
    {
        HANDLE_ERROR(ERROR_HASH, hashLen);
    }

#ifdef PRINT
    printBuffer("HASH", hash, hashLen);
#endif

#ifdef SIGN_DATA
    uint8_t signature[SIGNATURE_SIZE];
    int signatureLen = sign(hash, hashLen, signature, SIGNATURE_SIZE);
    
    if(signatureLen < 0)
    {
        HANDLE_ERROR(ERROR_SIGNATURE, signatureLen);
    }

#ifdef PRINT
    printBuffer("SIGN", signature, signatureLen);
#endif

    sendPkg(data, dataLen, signature, signatureLen);
#else
    sendPkg(data, dataLen, hash, hashLen);
#endif

#ifdef PRINT
    printf("SUCESSO\n");
#endif

    return 0;
}

int main()
{
#ifdef PRINT
    printf("INICIANDO TESTE SENDER\n");
#endif

    int i;
    for(i = 0; i < LOOP_COUNT; i++)
    {
#ifdef PRINT
        printf("LOOP %d\n", i);
#endif

        int result = test();

        if(result != 0)
            return result;
    }

#ifdef PRINT
    printf("TESTE SENDER FINALIZADO\n\n");
#endif

    return 0;
}
