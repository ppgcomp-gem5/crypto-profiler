#include <stdint.h>
#include <stddef.h>

void sendPkg(uint8_t* buffer, size_t bufferLen, uint8_t* extraData, size_t extraDataLen);
