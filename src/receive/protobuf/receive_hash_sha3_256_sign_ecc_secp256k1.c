#include <string.h>

#include "../receive.h"

uint8_t signature[] = 
{
    48, 69, 2, 33, 0, 235, 106, 86, 197, 166, 
    69, 26, 127, 95, 122, 112, 231, 233, 9, 197, 
    89, 80, 129, 187, 18, 158, 37, 47, 98, 69, 
    137, 181, 51, 216, 225, 215, 44, 2, 32, 77, 
    120, 242, 113, 98, 246, 253, 228, 249, 139, 5, 
    91, 223, 51, 67, 120, 139, 163, 225, 233, 71, 
    71, 198, 35, 215, 151, 105, 111, 252, 254, 245, 
    202,
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(signature))
        return extraDataLen - sizeof(signature);
        
    memcpy(extraData, signature, sizeof(signature));
    return sizeof(signature);
}
