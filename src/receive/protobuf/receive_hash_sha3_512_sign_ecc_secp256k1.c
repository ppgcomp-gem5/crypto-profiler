#include <string.h>

#include "../receive.h"

uint8_t signature[] = 
{
    48, 69, 2, 33, 0, 235, 106, 86, 197, 166, 
    69, 26, 127, 95, 122, 112, 231, 233, 9, 197, 
    89, 80, 129, 187, 18, 158, 37, 47, 98, 69, 
    137, 181, 51, 216, 225, 215, 44, 2, 32, 101, 
    214, 26, 89, 178, 71, 16, 232, 146, 84, 25, 
    4, 13, 50, 26, 14, 189, 177, 76, 224, 174, 
    37, 25, 148, 207, 119, 232, 233, 7, 45, 9, 
    93,
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(signature))
        return extraDataLen - sizeof(signature);
        
    memcpy(extraData, signature, sizeof(signature));
    return sizeof(signature);
}
