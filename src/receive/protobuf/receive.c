#include <string.h>

#include "../receive.h"

// {"data": "Exemplo de payload!"}
// Serializado utilizando protobuf através da ferramento online http://yura415.github.io/js-protobuf-encode-decode/

static uint8_t payload[] = 
{
    0x0A, 0x13, 0x45, 0x78, 0x65, 
    0x6D, 0x70, 0x6C, 0x6F, 0x20, 
    0x64, 0x65, 0x20, 0x70, 0x61, 
    0x79, 0x6C, 0x6F, 0x61, 0x64, 
    0x21,
};

int receiveData(uint8_t* data, size_t dataLen)
{
    if(dataLen < sizeof(payload))
        return dataLen - sizeof(payload);
        
    memcpy(data, payload, sizeof(payload));
    return sizeof(payload);
}
