#include <string.h>

#include "../receive.h"

static uint8_t hash[] = 
{ 
    31, 240, 92, 160, 101, 166, 179, 252, 175, 182, 
    249, 9, 209, 249, 220, 249, 151, 99, 204, 40, 
    42, 94, 90, 117, 19, 130, 103, 65, 171, 225, 
    38, 92,
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(hash))
        return extraDataLen - sizeof(hash);
        
    memcpy(extraData, hash, sizeof(hash));
    return sizeof(hash);
}
