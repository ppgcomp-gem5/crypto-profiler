#include <string.h>

#include "../receive.h"

static uint8_t hash[] = 
{ 
    51, 196, 120, 244, 29, 166, 181, 171, 226, 47, 
    180, 75, 229, 128, 64, 149, 248, 216, 216, 182, 
    97, 164, 54, 255, 177, 48, 215, 199, 193, 242, 
    127, 52, 
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(hash))
        return extraDataLen - sizeof(hash);
        
    memcpy(extraData, hash, sizeof(hash));
    return sizeof(hash);
}
