#include <string.h>

#include "../receive.h"

static uint8_t hash[] = 
{ 
    208, 68, 119, 53, 0, 198, 130, 107, 61, 139, 
    192, 7, 137, 18, 141, 14, 125, 222, 39, 83, 
    55, 100, 107, 60, 67, 245, 131, 36, 14, 9, 
    187, 22, 207, 234, 100, 213, 229, 148, 218, 98, 
    12, 248, 21, 27, 105, 144, 73, 28, 238, 48, 
    18, 5, 234, 21, 9, 113, 50, 102, 104, 216, 
    6, 94, 115, 242,
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(hash))
        return extraDataLen - sizeof(hash);
        
    memcpy(extraData, hash, sizeof(hash));
    return sizeof(hash);
}
