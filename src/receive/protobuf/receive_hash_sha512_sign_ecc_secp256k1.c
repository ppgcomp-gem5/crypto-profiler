#include <string.h>

#include "../receive.h"

uint8_t signature[] = 
{
    48, 69, 2, 33, 0, 235, 106, 86, 197, 166, 
    69, 26, 127, 95, 122, 112, 231, 233, 9, 197, 
    89, 80, 129, 187, 18, 158, 37, 47, 98, 69, 
    137, 181, 51, 216, 225, 215, 44, 2, 32, 61, 
    178, 12, 62, 124, 109, 123, 21, 204, 155, 89, 
    227, 19, 150, 236, 241, 54, 140, 52, 243, 122, 
    35, 0, 11, 90, 218, 248, 71, 197, 150, 12, 
    38,
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(signature))
        return extraDataLen - sizeof(signature);
        
    memcpy(extraData, signature, sizeof(signature));
    return sizeof(signature);
}
