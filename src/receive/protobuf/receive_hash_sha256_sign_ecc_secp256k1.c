#include <string.h>

#include "../receive.h"

uint8_t signature[] = 
{
    48, 69, 2, 33, 0, 235, 106, 86, 197, 166, 
    69, 26, 127, 95, 122, 112, 231, 233, 9, 197, 
    89, 80, 129, 187, 18, 158, 37, 47, 98, 69, 
    137, 181, 51, 216, 225, 215, 44, 2, 32, 39, 
    128, 82, 74, 181, 83, 55, 222, 233, 181, 121, 
    122, 76, 135, 118, 32, 250, 151, 64, 68, 111,
    251, 106, 215, 6, 216, 204, 190, 38, 118, 33, 
    81, 
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(signature))
        return extraDataLen - sizeof(signature);
        
    memcpy(extraData, signature, sizeof(signature));
    return sizeof(signature);
}
