#include <string.h>

#include "../receive.h"

static uint8_t hash[] = 
{ 
    46, 120, 158, 174, 39, 79, 34, 229, 238, 55, 
    166, 202, 36, 56, 20, 9, 187, 228, 193, 36, 
    226, 210, 207, 100, 129, 60, 111, 213, 20, 47, 
    90, 111, 181, 150, 52, 196, 126, 230, 215, 73, 
    140, 105, 154, 103, 183, 172, 246, 85, 152, 11, 
    2, 125, 78, 7, 186, 197, 251, 94, 231, 254, 
    20, 125, 240, 146,
};

int receiveExtraData(uint8_t* extraData, size_t extraDataLen)
{
    if(extraDataLen < sizeof(hash))
        return extraDataLen - sizeof(hash);
        
    memcpy(extraData, hash, sizeof(hash));
    return sizeof(hash);
}
