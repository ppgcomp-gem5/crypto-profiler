#include <stdint.h>
#include <stddef.h>

int receiveData(uint8_t* data, size_t dataLen);
int receiveExtraData(uint8_t* extraData, size_t extraDataLen);
