#include <stdint.h>
#include <stddef.h>

int sign(uint8_t* hash, size_t hashLen, uint8_t* out, size_t outLen);
int checkSignature(uint8_t* signature, size_t signatureLen, uint8_t* hash, size_t hashLen);