#include <stdio.h>
#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/asn_public.h>
#include <wolfssl/wolfcrypt/ecc.h>

#include "ecc_keys.h"
#include "../sign.h"

int sign(uint8_t* hash, size_t hashLen, uint8_t* out, size_t outLen)
{
    RNG rng;
    wc_InitRng(&rng);

    ecc_key key;
    word32 idx = 0;

    wc_ecc_init(&key);
    wc_ecc_set_curve(&key, 32, ECC_SECP256K1);
    int ret = wc_EccPrivateKeyDecode(ecc_private_key_sender, &idx, &key, sizeof(ecc_private_key_sender));

    if(ret < 0)
        return ret;

    ret = wc_ecc_sign_hash(hash, hashLen, out, &outLen, &rng, &key);

//    wc_ecc_key_free(&key);
//    wc_FreeRng(&rng);

    if(ret != 0)
        return ret;

    return outLen;
}

int checkSignature(uint8_t* signature, size_t signatureLen, uint8_t* hash, size_t hashLen)
{
    ecc_key key;
    word32 idx = 0;

    wc_ecc_init(&key);
    wc_ecc_set_curve(&key, 32, ECC_SECP256K1);
    int ret = wc_EccPublicKeyDecode(ecc_public_key_sender, &idx, &key, sizeof(ecc_public_key_sender));

    if(ret < 0)
        return ret;

    int stat;
    ret = wc_ecc_verify_hash(signature, signatureLen, hash, hashLen, &stat, &key);

    //wc_ecc_key_free(&key);

    if(ret < 0)
        return ret;

    if(stat != 1)
        return -2;

    return hashLen;
}
