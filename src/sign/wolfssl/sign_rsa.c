#include <stdio.h>
#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/rsa.h>

#include "../sign.h"
#include "rsa_keys.h"
#include "../../print.h"

const int signatureSize;

int sign(uint8_t* hash, size_t hashLen, uint8_t* out, size_t outLen)
{
    RsaKey key;
    int32_t idx = 0;

    RNG rng;
    wc_InitRng(&rng);

    wc_InitRsaKey(&key, NULL);
    wc_RsaSetRNG(&key, &rng);
    wc_RsaPrivateKeyDecode(rsa_private_key, &idx, &key, sizeof(rsa_private_key));

    int len = wc_RsaSSL_Sign(hash, 32, out, outLen, &key, &rng);

    wc_FreeRsaKey(&key);
    wc_FreeRng(&rng);

    return len;
}

int checkSignature(uint8_t* signature, size_t signatureLen, uint8_t* hash, size_t hashLen)
{
    RsaKey key;
    int32_t idx = 0;
    
    wc_InitRsaKey(&key, NULL);
    wc_RsaPublicKeyDecode(rsa_public_key, &idx, &key, sizeof(rsa_public_key));

    uint8_t receivedHash[hashLen];
    int receivedHashLen = wc_RsaSSL_Verify(signature, signatureLen, receivedHash, hashLen, &key);

    if(receivedHashLen < 0)
        return receivedHashLen;

#ifdef PRINT
    printBuffer("RECV_HASH", receivedHash, receivedHashLen);
#endif

    int len = hashLen;
    if(receivedHashLen < hashLen)
        len = receivedHashLen;

    int i;
    for(i=0; i < len; i++)
        if(receivedHash[i] != hash[i])
            return -2;

    wc_FreeRsaKey(&key);

    return receivedHashLen;
}
