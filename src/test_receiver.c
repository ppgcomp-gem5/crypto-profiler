#include "data/data.h"
#include "errors.h"
#include "hash/hash.h"
#include "receive/receive.h"
#include "sign/sign.h"

#ifdef PRINT
#include <stdio.h>
#include "print.h"
#endif

#define DATA_SIZE 256
#define EXTRA_DATA_SIZE 512
#define HASH_SIZE   128

#ifndef LOOP_COUNT
#define LOOP_COUNT  1
#endif

int test()
{
    uint8_t data[DATA_SIZE];
    int dataLen = receiveData(data, DATA_SIZE);

    if(dataLen < 0)
    {
        HANDLE_ERROR(ERROR_DATA, dataLen);
    }

#ifdef PRINT
    printBuffer("DATA", data, dataLen);
#endif

    uint8_t extraData[EXTRA_DATA_SIZE];
    int extraDataLen = receiveExtraData(extraData, EXTRA_DATA_SIZE);

    if(extraDataLen < 0)
    {
        HANDLE_ERROR(ERROR_DATA, extraDataLen);
    }

#ifdef PRINT
    printBuffer("EXTRA_DATA", extraData, extraDataLen);
#endif

    uint8_t calculatedHash[HASH_SIZE];
    int calculatedHashLen = calcHash(data, dataLen, calculatedHash, HASH_SIZE);

    if(calculatedHashLen < 0)
    {
        HANDLE_ERROR(ERROR_HASH, calculatedHashLen);
    }

#ifdef PRINT
    printBuffer("CALC_HASH", calculatedHash, calculatedHashLen);
#endif

#ifdef SIGN_DATA
    int receivedHashLen = checkSignature(extraData, extraDataLen, calculatedHash, calculatedHashLen);

    if(receivedHashLen < 0)
    {
        HANDLE_ERROR(ERROR_SIGNATURE, receivedHashLen);
    }
#else
    uint8_t* receivedHash = extraData;
    int receivedHashLen = extraDataLen;

    if(receivedHashLen != calculatedHashLen)
    {
        HANDLE_ERROR(ERROR_HASH, -1);
    }

    int i;
    for(i=0; i < calculatedHashLen; i++)
    {
        if(receivedHash[i] != calculatedHash[i])
        {
            HANDLE_ERROR(ERROR_HASH, -2);
        }
    }
#endif

    processData(data, dataLen);

#ifdef PRINT
    printf("SUCESSO\n");
#endif

    return 0;
}

int main()
{
#ifdef PRINT
    printf("INICIANDO TESTE RECEIVER\n");
#endif

    int i;
    for(i = 0; i < LOOP_COUNT; i++)
    {
#ifdef PRINT
        printf("LOOP %d\n", i);
#endif

        int result = test();

        if(result != 0)
            return result;
    }

#ifdef PRINT
    printf("TESTE RECEIVER FINALIZADO\n\n");
#endif

    return 0;
}
