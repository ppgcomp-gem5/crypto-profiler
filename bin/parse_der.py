#!/usr/bin/python

def main():
    fh = open('/workspaces/crypto-profiler/keys/rsa_public.der', 'rb')
    try:
        byte = fh.read(1)
        while byte != "":
            print '0x%02x,' % ord(byte),
            byte = fh.read(1)
    finally:
        fh.close

    fh.close()

if __name__ == "__main__":
        main()