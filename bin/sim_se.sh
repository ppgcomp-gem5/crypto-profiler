#!/bin/bash

WORKSPACE_FOLDER='/workspaces/crypto-profiler'

GEM5_FOLDER=$WORKSPACE_FOLDER/sim/gem5
GEM5_SIM=${GEM5_FOLDER}/build/ARM/gem5.opt
GEM5_SYSTEM=${GEM5_FOLDER}/configs/example/arm/starter_se.py
GEM5_CPU='hpi'

BENCHMARKS_FOLDER=$WORKSPACE_FOLDER/benchmarks/se

OUTPUTS_FOLDER=$WORKSPACE_FOLDER/build/arm
OUTPUTS=$(find $OUTPUTS_FOLDER -type f -name "*.out")

for entry in ${OUTPUTS[*]}; 
do
    NAME=$(basename -s .out ${entry})
    $GEM5_SIM -d $BENCHMARKS_FOLDER/$NAME $GEM5_SYSTEM --cpu="${GEM5_CPU}" $entry
done

# ../gem5/build/ARM/gem5.opt ../gem5/configs/example/arm/starter_se.py --cpu="minor" test_receiver_sha256_wolfssl_ecc_secp256k1_wolfssl.out
