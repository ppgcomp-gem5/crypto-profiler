#!/bin/bash

cd /workspaces/crypto-profiler

OUTPUTS_FOLDER='build'
OUTPUTS=$(find $OUTPUTS_FOLDER -type f -name "*.out")

for entry in ${OUTPUTS[*]}; 
do
    NAME=$(basename -s .out ${entry})
    echo TESTANDO $NAME -----------------------------------
    $entry
    echo
done
