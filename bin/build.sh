#!/bin/bash

WORKSPACE_FOLDER='/workspaces/crypto-profiler'
SRC_FOLDER=$WORKSPACE_FOLDER/src

if [ "$1" = "arm" ];
then
    COMPILER='arm-linux-gnueabihf-gcc'
    COMPILER_FLAGS='--static'
    OUTPUT_FOLDER=$WORKSPACE_FOLDER/build/arm
else
    COMPILER='gcc'
    COMPILER_FLAGS=''
    OUTPUT_FOLDER=$WORKSPACE_FOLDER/build/current
fi

rm -rf $OUTPUT_FOLDER
mkdir -p $OUTPUT_FOLDER

LINKER_DEFINES='-DPRINT'
# LINKER_DEFINES=''
HASH_LINKER_DEFINES=''
SIGN_LINKER_DEFINES='-DSIGN_DATA'

LINKER_FLAGS='-lwolfssl'
HASH_LINKER_FLAGS=''
SIGN_LINKER_FLAGS='-lm -lpthread'

# Lista as pastas de implementações dos dados.
DATA_IMPLEMENTATIONS=$(find $SRC_FOLDER/data -mindepth 1 -maxdepth 1 -type d)

# Lista as pastas de implementações dos hashes
HASH_LIBRARIES=$(find $SRC_FOLDER/hash -mindepth 1 -maxdepth 1 -type d)

# Lista as pastas de implementações das assinaturas
SIGN_LIBRARIES=$(find $SRC_FOLDER/sign -mindepth 1 -maxdepth 1 -type d)

for HASH_LIBRARY in ${HASH_LIBRARIES[*]};
do
    HASH_LIBRARY_NAME=$(basename ${HASH_LIBRARY})

    # Lista as implementações de hash dentro da biblioteca.
    HASHES=$(find $HASH_LIBRARY -maxdepth 1 -type f -name "hash_*.c")
    
    for HASH in ${HASHES[*]};
    do
        HASH_NAME=$(basename -s .c ${HASH})

        for DATA in ${DATA_IMPLEMENTATIONS[*]};
        do
            DATA_NAME=$(basename -s .c ${DATA})

            # Compilando os testes somente com hash (sem assinatura)
            $COMPILER $COMPILER_FLAGS $SRC_FOLDER/test_sender.c $DATA/data.c $HASH $SRC_FOLDER/send/mock/send.c $SRC_FOLDER/print.c $LINKER_DEFINES $HASH_LINKER_DEFINES $LINKER_FLAGS $HASH_LINKER_FLAGS -o $OUTPUT_FOLDER/test_sender_${DATA_NAME}_${HASH_NAME}_${HASH_LIBRARY_NAME}.out
            $COMPILER $COMPILER_FLAGS $SRC_FOLDER/test_receiver.c $SRC_FOLDER/receive/$DATA_NAME/receive.c $SRC_FOLDER/receive/$DATA_NAME/receive_${HASH_NAME}.c $HASH $DATA/data.c $SRC_FOLDER/print.c $LINKER_DEFINES $HASH_LINKER_DEFINES $LINKER_FLAGS $HASH_LINKER_FLAGS -o $OUTPUT_FOLDER/test_receiver_${DATA_NAME}_${HASH_NAME}_${HASH_LIBRARY_NAME}.out

            for SIGN_LIBRARY in ${SIGN_LIBRARIES[*]};
            do
                SIGN_LIBRARY_NAME=$(basename ${SIGN_LIBRARY})

                # Lista as implementações de assinatura dentro da biblioteca.
                SIGNS=$(find $SIGN_LIBRARY -maxdepth 1 -type f -name "sign_*.c")

                for SIGN in ${SIGNS[*]};
                do
                    SIGN_NAME=$(basename -s .c ${SIGN})

                    $COMPILER $COMPILER_FLAGS $SRC_FOLDER/test_sender.c $DATA/data.c $HASH $SIGN $SRC_FOLDER/send/mock/send.c $SRC_FOLDER/print.c $LINKER_DEFINES $SIGN_LINKER_DEFINES $LINKER_FLAGS $SIGN_LINKER_FLAGS -o $OUTPUT_FOLDER/test_sender_${DATA_NAME}_${HASH_NAME}_${HASH_LIBRARY_NAME}_${SIGN_NAME}_${SIGN_LIBRARY_NAME}.out
                    $COMPILER $COMPILER_FLAGS $SRC_FOLDER/test_receiver.c $SRC_FOLDER/receive/$DATA_NAME/receive.c $SRC_FOLDER/receive/$DATA_NAME/receive_${HASH_NAME}_${SIGN_NAME}.c $HASH $SIGN $DATA/data.c $SRC_FOLDER/print.c $LINKER_DEFINES $SIGN_LINKER_DEFINES $LINKER_FLAGS $SIGN_LINKER_FLAGS -o $OUTPUT_FOLDER/test_receiver_${DATA_NAME}_${HASH_NAME}_${HASH_LIBRARY_NAME}_${SIGN_NAME}_${SIGN_LIBRARY_NAME}.out
                done;
            done;
        done;
    done;
done;
